module ByteSequenceHelper

  def clean(body)
    body
      .encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
  end
end