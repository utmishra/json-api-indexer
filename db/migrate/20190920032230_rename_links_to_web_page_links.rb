class RenameLinksToWebPageLinks < ActiveRecord::Migration[5.2]
  def change
    rename_table :links, :web_page_links
  end
end
