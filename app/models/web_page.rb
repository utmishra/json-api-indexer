class WebPage < ApplicationRecord
  has_many :web_page_links

  validates :url, presence: true, url: true
end
