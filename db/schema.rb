# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_09_20_034836) do

  create_table "web_pages", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "url"
    t.string "h1"
    t.string "h2"
    t.string "h3"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "web_page_id"
    t.index ["web_page_id"], name: "index_web_pages_on_web_page_id"
  end

  create_table "web_pages_links", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "title"
    t.string "href"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "web_page_id"
    t.index ["web_page_id"], name: "fk_rails_fca428e691"
  end

  add_foreign_key "web_pages", "web_pages"
  add_foreign_key "web_pages_links", "web_pages"
end
