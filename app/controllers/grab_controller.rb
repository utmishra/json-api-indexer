class GrabController < ApplicationController

  require "utf8_utils"
  require 'nokogiri'
  require 'open-uri'


  def index
    url = params[:url]
    begin
      if !url.nil?
        doc = Nokogiri::HTML(open(url))
        h1_xpath = doc.xpath('//h1').first
        h2_xpath = doc.xpath('//h2').first
        h3_xpath = doc.xpath('//h3').first
        h1 = h1_xpath.text if !h1_xpath.nil?
        h2 = h2_xpath.text if !h2_xpath.nil?
        h3 = h3_xpath.text if !h3_xpath.nil?
        web_page = WebPage.new(url: url, h1: h1, h2: h2, h3: h3)
        if web_page.save
          doc.xpath('//a').each do |link|
            # p link['href']
            # p link.text
            # p web_page.id
            unless link.text.nil? || link['href'].nil?
              link_text = link.text.tidy_bytes
              link_url  = link['href'].tidy_bytes
              web_page_link = WebPagesLink.create(href: link['href'], title: link_text, web_page_id: web_page.id)
            end            
          end
        else 
          p web_page.errors
          render json: { "status" => 401, "message" => web_page.errors }
        end
        render json: web_page
      else
        render json: { status => 400, message: 'Invalid input'}
      end
    rescue => exception
      p exception
      render json: { "status" => 500, "message": 'Something went wrong.'}
    end
  end
end