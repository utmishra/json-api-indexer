class WebPageResource < JSONAPI::Resource
  attributes :url, :h1, :h2, :h3
  has_many :web_pages_links
end