Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :web_pages
  get '/grab',  to: 'grab#index'
  jsonapi_resources :web_pages
end
