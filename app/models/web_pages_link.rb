class WebPagesLink < ApplicationRecord
  belongs_to :web_page

  validates :title, presence: true
  validates :href, presence: true, url: true
end
