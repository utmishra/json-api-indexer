class AddLinksReferencesToWebPage < ActiveRecord::Migration[5.2]
  def change
    add_reference :web_pages, :web_page, foreign_key: true
  end
end
