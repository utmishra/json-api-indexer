class WebPagesLinkResource < JSONAPI::Resource
  attributes :title, :href
  has_one :web_page
end