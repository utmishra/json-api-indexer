# README

RESTful Json API to index a page's content

## Purpose
- This app takes URL as a parameter and indexes it's main contents (Headings, links) into database.
- Target database: MySQL

## API specs
- Index URL

### Endpoint:

- Fetch single page

`GET /grab?url=<https://sampleurl.com`

Response:

```
  {
    "id": 15,
    "url": "https://youtube.com",
    "h1": "Youtube Inc.,
    "h2": "Home",
    "h3": "Best of YouTube",
    "created_at": "2019-09-20T06:10:57.000Z",
    "updated_at": "2019-09-20T06:10:57.000Z",
    "web_page_id": null
  }
```

- Get all web pages

`GET /web_pages`

Response:

```
    {
      "data": [
        {
          "id": "11",
          "type": "web-pages",
          "links": {
            "self": "http://localhost:3000/web-pages/11"
          },
          "attributes": {
            "url": "https://google.com",
            "h1": null,
            "h2": null,
            "h3": null
          },
          "relationships": {
            "web-pages-links": {
              "links": {
                "self": "http://localhost:3000/web-pages/11/relationships/web-pages-links",
                "related": "http://localhost:3000/web-pages/11/web-pages-links"
              }
            }
          }
        }
        ...
      ]
    }
```




Things you may want to cover:

* Ruby version: `2.6.3p62`

* Important gems:
    * Nokigiri
    * JSON:API resource
    * MySQL2

* Database creation: `rails db:create`