class CreateWebPages < ActiveRecord::Migration[5.2]
  def change
    create_table :web_pages do |t|
      t.string :url
      t.string :h1
      t.string :h2
      t.string :h3

      t.timestamps
    end
  end
end
