class AddWebPagesLinksForeignKey < ActiveRecord::Migration[5.2]
  def change
    add_column :web_pages_links, :web_page_id, :bigint
    add_foreign_key :web_pages_links, :web_pages
  end
end
